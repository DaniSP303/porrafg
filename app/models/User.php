<?php

class User{

private $db;

private $name;

private $email;

private $password;

private $userType;

private $score;

private $avatar;


public function __construct($nick = '', $email = '', $password = '', $userType = '', $score = '' ,$avatar = ''){

    $this->db = new Database();
    $this->nick = $nick;
    $this->email = $email;   
    $this->password = $password;
    $this->userType = $userType;
    $this->score = $score;
    $this->avatar = $avatar;

}

public function getNick(){

    return $this->nick;

}

public function getEmail(){
    
    return $this->email;

}

public function getPassword(){

    return $this->password;
    
}

public function getUserType(){

    return $this->userType;
    
}

public function getScore(){

    return $this->score;
    
}

public function getAvatar(){

    return $this->avatar;
    
}

public function setNick($nick){

     $this->nick = $nick;

}

public function setEmail($email){
    
    $this->email = $email;

}

public function setPassword($password){

    $this->password = $password;
    
}

public function setUserType($userType){

    $this->userType = $userType;
    
}

public function setScore($score){

    $this->score = $score;
    
}

public function setAvatar($avatar){

     $this->avatar = $avatar;
    
}


public function findUserByEmail($mail){

 $this->db->query('SELECT * FROM users where email = :email');

 $this->db->bind(':email', $mail);

 $results = $this->db->resultOne();

 return $results;
}

public function userExist($mail){

    $this->db->query('SELECT * FROM users where email = :email');
   
    $this->db->bind(':email', $mail);
   
    $results = $this->db->resultSet("User");
   
    return $results;
   }

public function register($data){

 $this->db->query('insert into users (nick,email,password, userType, score, avatar) Values (:nk,:ml,:pw, :ut, :sc, :av)');

 $this->db->bind(':nk', $data['nick']);

 $this->db->bind(':ml', $data['email']);

 $this->db->bind(':pw', $data['password']);

 $this->db->bind(':ut', $data['userType']);

 $this->db->bind(':sc', $data['score']);

 $this->db->bind(':av', $data['avatar']);

 return $this->db->execute();

}

public function login($data){

    $result = $this->findUserByEmail($data['email']);
    var_dump($result);
   
    if(password_verify($data['password'], $result->password)){
        return $result;
    }else{
        return false;
    }
   
   }


public function rowCount($mail){

return  count($this->userExist($mail));
 
}

}
?>