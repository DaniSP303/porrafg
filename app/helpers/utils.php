<?php 

class Utils {

    function containsURL($url){
        if(str_contains($_SERVER['REQUEST_URI'], $url)){
            return "active";
        }
     }

public function helperDD($arg){

    var_dump($arg) and die();
}

public function urlHelper($route){

    header("Location: ".URLROOT.$route) and die();
}

public function createUserSession($user){
   $_SESSION['id'] = $user->id;
   $_SESSION['name'] = $user->name;
   $_SESSION['email'] = $user->email;
   $_SESSION['userType'] = $user->userType;
   $this->urlHelper("posts/index/");
}

public function logout(){
    unset($_SESSION['id']);
    unset($_SESSION['name']);
    unset($_SESSION['email']);
    unset($_SESSION['userType']);
    session_destroy();
    $this->urlHelper("users/login");
}

public function isLoggedIn(){

    if(isset($_SESSION['id'])){
        return true;
    }else{
        return false;
    }

}

}
?>