<?php include_once APPROOT. "/views/partials/header.php"; ?>
<?php include_once APPROOT. "/views/partials/navbar.php"; ?>

<a class="btn btn-warning pull-right" href="<?= URLROOT. "posts/show/?post_id=".$_GET['post_id'] ?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<?= (string) flash() ?>
<div class="card card-body bg-light mt-5">
    <h2>Editar publicación</h2>
    <p>Por favor introduzca los cambios de su publicación</p>
    <form method="POST">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?php if ($data['title_err'] != ""){ echo "is-invalid";}?>" placeholder="Título de la publicación" value="<?= $data['title']?>">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?php if ($data['body_err'] != ""){ echo "is-invalid";}?>" rows="5" placeholder="Su contenido"><?= $data['body']?></textarea>

            <span class="invalid-feedback"></span>
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="editar publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php include_once APPROOT. "/views/partials/footer.php"; ?>