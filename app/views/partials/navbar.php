<?php $helper = new Utils; ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item <?= $helper->containsURL('index')?>">
        <a class="nav-link" href="<?php if($helper->isLoggedIn()){ echo URLROOT.'posts/index';}else{ echo URLROOT.'paginas/index';} ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item <?= ($helper->containsURL('about') ? 'active' : ''); ?>">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    <div class="my-2 my-lg-0">
    <p><?php if(isset($_SESSION['name'])) echo "Bienvenido ".$_SESSION['name'];?></p>
    <ul class="navbar-nav mr-auto">
<?php


if($helper->isLoggedIn()){


     echo       '<li class="nav-item">
     <a class="nav-link '.$helper->containsURL('logout').'" href="'.URLROOT.'users/logout">Logout</a>
   </li>';
  }
  else{
    echo '<li class="nav-item">
    <a class="nav-link '.$helper->containsURL('login').'" href="'.URLROOT.'users/login"">Log In</a>
  </li>
  <li class="nav-item">
    <a class="nav-link '.$helper->containsURL('register').'" href="'.URLROOT.'users/register">Register</a>
  </li>';
  }
?>


    </ul>
    </div>
  </div>
</nav>