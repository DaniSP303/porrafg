<?php
use \Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller{

  
    

    public function __construct() {
   
    $help = new Utils;
    if($help->isLoggedIn()){

    }else{
        $flash = new Flash();
        $flash->message('Acceso denegado, inicie sesisón', 'warning');
        $help->urlHelper('users/login');
    }

    $this->post = $this->model("Post");
   
    }

    public function index()
    {
        $data = $this->post->getPosts();
        return $this->view('posts/index', $data);
    }

    public function show(){

        $data = $this->post->getPost($_GET['post_id']);
        return $this->view('posts/show', $data);
    }

    public function add(){

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'title' => '',
                'body' => '',
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => '',
                ];

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data['title'] = trim($_POST['title']);
            $data['body'] = trim($_POST['body']);

            if($data['title'] == ""){
                $data['title_err'] = "Hace falta un título";
            }else{
                $data['title_err'] = "";
            }

            if($data['body'] == ""){
                $data['body_err'] = "El cuerpo del post no puede estar vacío";
            }else{
                $data['body_err'] = "";
            }

            if (!empty($data['image'])) {
                try {

                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    $file = new File($_FILES['image'],$arrTypes);
                    $file->fileChecker();
                    
                    }catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                    }

                }

            if( $data['title_err'] == "" && $data['body_err'] == "" && $data['image_err'] == ""){


                $result = $this->post->addPost($data);
                if ($result){
                    $helper = new Utils;
                    $flash = new Flash();
                    $flash->message('¡Post creado!.', 'info');
                    $helper->urlHelper("posts/index");
                }else{
                    $helper = new Utils;
                    $flash = new Flash();
                    $flash->message('Se ha producido un error', 'error');
                    $this->view('posts/add', $data);
                }

            }
            else    
            {
               $this->view('posts/add', $data);
            }
            
        } else {
            $data = [
            'title' => '',
            'body' => '',
            'title_err' => '',
            'body_err' => '',
            ];
        $this->view('posts/add', $data);
        }

    }

    public function edit(){
        $post = $this->post->getPost($_GET['post_id']);
        if($post->userId == $_SESSION['id']){


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $data = [
                'id' => $_GET['post_id'],
                'title' => '',
                'body' => '',
                'title_err' => '',
                'body_err' => '',
                ];

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data['title'] = trim($_POST['title']);
            $data['body'] = trim($_POST['body']);

            if($data['title'] == ""){
                $data['title_err'] = "Hace falta un título";
            }else{
                $data['title_err'] = "";
            }

            if($data['body'] == ""){
                $data['body_err'] = "El cuerpo del post no puede estar vacío";
            }else{
                $data['body_err'] = "";
            }

            if( $data['title_err'] == "" && $data['body_err'] == ""){


                
                $result = $this->post->updatePost($data);
                if ($result){
                    $helper = new Utils;
                    $flash = new Flash();
                    $flash->message('¡Post Actualizado!.', 'info');
                    $helper->urlHelper("posts/show/?post_id=".$data['id']);
                }else{
                    $helper = new Utils;
                    $flash = new Flash();
                    $flash->message('Se ha producido un error', 'error');
                    $this->view('posts/edit'.$post->postId, $data);
                }

            }
            else    
            {
                $this->view('posts/edit', $data);
            }
            
        } else {
            $data = [
                'title' => $post->title,
                'body' => $post->body,
                'title_err' => '',
                'body_err' => '',
                ];
    
       $this->view('posts/edit', $data);
        }

    }else{
        $helper = new Utils;
        $flash = new Flash();
        $flash->message('Acceso denegado', 'warning');
        $helper->urlHelper('posts/index');

    }

}

public function delete(){

    $post = $this->post->getPost($_POST['post_id']);
    if($post->userId == $_SESSION['id']){

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->post->deletePost($_POST['post_id']);
            $helper = new Utils;
            $flash = new Flash();
            $flash->message('Publicación borrada', 'info');
            $helper->urlHelper('posts/index');
        }else{
            $helper = new Utils;
            $flash = new Flash();
            $flash->message('Operación denegada', 'warning');
            $helper->urlHelper('posts/index');
        }
    }else{
        $helper = new Utils;
        $flash = new Flash();
        $flash->message('Operación denegada', 'warning');
        $helper->urlHelper('posts/index');
    }
}

   
   
   }

?>